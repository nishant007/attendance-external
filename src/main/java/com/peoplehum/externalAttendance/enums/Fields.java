package com.peoplehum.externalAttendance.enums;

public enum Fields {
  ATTENDANCEDATE("AttendanceDate"),
  EMPLOYEEID("EmployeeId"),
  INTIME("InTime"),
  OUTTIME("OutTime"),
  DURATION("Duration"),
  STATUS("Status");

  private String value;

  Fields(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
