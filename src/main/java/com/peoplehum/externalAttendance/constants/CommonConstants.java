package com.peoplehum.externalAttendance.constants;

import com.squareup.okhttp.MediaType;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CommonConstants {
  public static final String ATTENDANCE_ENTITY_TYPE = "CHECKIN_CHECKOUT";
  public static final String REQUEST_HEADER_CUSTOMER_ID = "customerId";
  public static final String REQUEST_HEADER_USER_ID = "userId";
  public static final String REQUEST_HEADER_ROLE_IDS = "roleIds";
  public static final String IDENTIFICATION_TYPE = "EMPLOYEE_ID";

  public static final String REQUEST_HOST = "https://devapi.peoplehum.com/leave-management/";
  public static final String VERSION = "v1.0/";
  public static final String PATH = "customer/{0}/attendance/bulk";

  public static final Long CUSTOMER_ID = 10142L;
  public static final Long USER_ID = 16461L;
  public static final Set<Long> ROLE_IDS = new HashSet<>(Arrays.asList(4L, 2L));
  public static final Long TIMEZONE = 330L;

  public static final String AUTHORIZATION = "Authorization";
  public static final String authToken = "Basic d64b0542-8c80-4750-b58b-d673027f278d-EHegJMo";
  public static final MediaType JSON = MediaType.parse("application/json");

}
