package com.peoplehum.externalAttendance.executor;

import com.peoplehum.externalAttendance.constants.CommonConstants;
import com.peoplehum.externalAttendance.entity.AttendanceLog;
import com.peoplehum.externalAttendance.enums.Fields;
import com.peoplehum.externalAttendance.enums.Status;
import com.peoplehum.externalAttendance.model.AttendanceResponse;
import com.peoplehum.externalAttendance.model.ExternalAttendanceModel;
import com.peoplehum.externalAttendance.util.PeoplehumAttendanceUtility;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@EnableAsync
@EnableScheduling
@Component("com.peoplehum.externalAttendance.executor.AttendanceLogsPicker")
@PropertySource("classpath:application.properties")
public class AttendanceLogsPicker implements EnvironmentAware{

  @Autowired
  static Environment environment;

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Override
  public void setEnvironment(final Environment environment) {
    AttendanceLogsPicker.environment = environment;
  }

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  @Qualifier("com.peoplehum.externalAttendance.util.PeoplehumAttendanceUtility")
  private PeoplehumAttendanceUtility peoplehumAttendanceUtility;


  @Scheduled(cron = "${scheduled.job.attendance.logs.cron}", zone = "UTC")
  public void attendanceLogsExecutor() {

    log.info("attendance logs executor started at :{}", LocalDateTime.now());

    // calling method to process attendance data
    Date searchDate = Date.valueOf(LocalDateTime.now().toLocalDate().minusDays(1L));
    processAttendanceLogs(searchDate, searchDate);

    log.info("attendance logs executor finished at :{}", LocalDateTime.now());
  }

  public void loadAndSendAttendanceLogs(Date fromDate, Date toDate) {
    log.info("request accepted to proccess attendance logs from date :{} to date :{}", fromDate,
        toDate);
    // calling method to process attendance data
    processAttendanceLogs(fromDate, toDate);
  }

  private void processAttendanceLogs(Date fromDate, Date toDate) {

    //setting datasource
    setDataSource();

    String query =
        "SELECT AttendanceDate, EmployeeId, InTime, OutTime, Duration, Status FROM AttendanceLogs"
            + " WHERE (((AttendanceDate)>=#" + fromDate + "# And (AttendanceDate)<=#" + toDate
            + "#))";
    List<AttendanceLog> attendanceLogList = jdbcTemplate.query(query,
        (rs, rowNum) -> new AttendanceLog(rs.getDate(Fields.ATTENDANCEDATE.getValue()),
            rs.getString(Fields.EMPLOYEEID.getValue()), rs.getString(Fields.INTIME.getValue()),
            rs.getString(Fields.OUTTIME.getValue()), rs.getString(Fields.DURATION.getValue()),
            rs.getString(Fields.STATUS.getValue())));

    if (!CollectionUtils.isEmpty(attendanceLogList)) {
      AttendanceResponse attendanceResponse = new AttendanceResponse();
      List<ExternalAttendanceModel> externalAttendanceModelList = new ArrayList<>();
      Set<String> allowedStatuses = new HashSet<>(Arrays
          .asList(Status.Absent.getValue(), Status.Present.getValue(),
              Status.WeeklyOffPresent.getValue(), Status.HalfDayPresent.getValue()));
      for (AttendanceLog attendanceLog : attendanceLogList) {

        attendanceLog.setStatus(attendanceLog.getStatus().trim().replaceAll(" ", ""));
        if (allowedStatuses.contains(attendanceLog.getStatus().trim())) {
          log.info("processing data to model for Sunny Diamonds's employee :{}",
              attendanceLog.getEmployeeId());

          ExternalAttendanceModel model = new ExternalAttendanceModel();
          model.setId(attendanceLog.getEmployeeId());
          model.setMarkedOn(attendanceLog.getAttendanceDate());

          //setting status
          setStatus(attendanceLog, model);

          if (model.getAttendanceStatus().equals(Status.PRESENT.getValue())) {
            model.setCheckInTime(Timestamp.valueOf(attendanceLog.getInTime()));
            model.setCheckOutTime(Timestamp.valueOf(attendanceLog.getOutTime()));

            Double timeInMinutes = Double.parseDouble(attendanceLog.getDuration());
            DecimalFormat decimalFormat = new DecimalFormat("##.00");
            Double totalWorkedTime = Double.parseDouble(decimalFormat.format(timeInMinutes / 60.0));
            model.setTotalTimeWorked(totalWorkedTime);
          }

          model.setAttendanceEntityType(CommonConstants.ATTENDANCE_ENTITY_TYPE);
          model.setTimezone(CommonConstants.TIMEZONE);
          model.setSourceId(Long.parseLong(attendanceLog.getEmployeeId()));

          externalAttendanceModelList.add(model);
        }
      }

      attendanceResponse.setIdentificationType(CommonConstants.IDENTIFICATION_TYPE);
      attendanceResponse.setExternalUserAttendanceModels(externalAttendanceModelList);
      peoplehumAttendanceUtility = new PeoplehumAttendanceUtility();
      //calling rest client to send data
      peoplehumAttendanceUtility.upsertBulkAttendance(attendanceResponse);
    }
  }

  private void setDataSource() {
    String dataSourceDriver = environment.getProperty("spring.datasource.driver-class-name");
    String dataSourceUrl = environment.getProperty("spring.datasource.url");
    DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
    driverManagerDataSource.setDriverClassName(dataSourceDriver);
    driverManagerDataSource.setUrl(dataSourceUrl);

    jdbcTemplate = new JdbcTemplate();
    jdbcTemplate.setDataSource(driverManagerDataSource);
  }

  private void setStatus(AttendanceLog log, ExternalAttendanceModel model) {
    Status status = getEnum(log.getStatus());
    switch (status) {
      case Present:
      case WeeklyOffPresent:
      case HalfDayPresent:
        model.setAttendanceStatus(Status.PRESENT.getValue());
        break;
      case Absent:
        model.setAttendanceStatus(Status.ABSENT.getValue());
        break;
    }
  }

  private Status getEnum(String value) {
    Optional<Status> statusEnum = Arrays.stream(Status.values())
        .filter(status -> status.getValue().trim().equals(value.trim())).findFirst();
    return statusEnum.orElse(null);
  }
}
