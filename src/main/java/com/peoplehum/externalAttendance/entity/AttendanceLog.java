package com.peoplehum.externalAttendance.entity;

import lombok.*;

import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString(doNotUseGetters = true)
public class AttendanceLog {
  private Date AttendanceDate;
  private String EmployeeId;
  private String InTime;
  private String OutTime;
  private String Duration;
  private String Status;

  public AttendanceLog(Date attendanceDate, String employeeId, String inTime, String outTime,
      String duration, String status) {
    AttendanceDate = attendanceDate;
    EmployeeId = employeeId;
    InTime = inTime;
    OutTime = outTime;
    Duration = duration;
    Status = status;
  }
}
