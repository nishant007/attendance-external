package com.peoplehum.externalAttendance.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Date;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ExternalAttendanceModel {

  private String id;
  private Long userId;
  private Date markedOn;
  private Long sourceId;
  private Timestamp checkInTime;
  private Timestamp checkOutTime;
  private String attendanceStatus;
  private String attendanceEntityType;
  private Long entityId;
  private Long timezone;
  private Double totalTimeWorked;

}