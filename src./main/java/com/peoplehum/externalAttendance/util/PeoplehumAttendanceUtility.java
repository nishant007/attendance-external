package com.peoplehum.externalAttendance.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.peoplehum.externalAttendance.constants.CommonConstants;
import com.peoplehum.externalAttendance.model.AttendanceResponse;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component("com.peoplehum.externalAttendance.util.PeoplehumAttendanceUtility")
public class PeoplehumAttendanceUtility {

  private ObjectMapper mapper;
  private OkHttpClient httpClient;

  public void upsertBulkAttendance(AttendanceResponse attendanceResponse) {
    log.info("sending attendance data for customer :{}, at :{}", 2569, LocalDateTime.now());
    try {
      httpClient = new OkHttpClient();
      mapper = new ObjectMapper();
      //BODY
      RequestBody requestBody = RequestBody
          .create(CommonConstants.JSON, this.mapper.writeValueAsString(attendanceResponse));
      //URL
      String url =
          new StringBuffer().append(CommonConstants.REQUEST_HOST).append(CommonConstants.VERSION)
              .append(MessageFormat
                  .format(CommonConstants.PATH, String.valueOf(CommonConstants.CUSTOMER_ID)))
              .toString();

      //HEADERS
      Map<String, String> requestHeaders = new HashMap<>();
      requestHeaders.put(CommonConstants.REQUEST_HEADER_CUSTOMER_ID,
          this.mapper.writeValueAsString(CommonConstants.CUSTOMER_ID));
      requestHeaders.put(CommonConstants.REQUEST_HEADER_USER_ID,
          this.mapper.writeValueAsString(CommonConstants.USER_ID));
      requestHeaders.put(CommonConstants.REQUEST_HEADER_ROLE_IDS,
          CommonConstants.ROLE_IDS.toString().replaceAll("\\[", "").replaceAll("]", ""));
      requestHeaders.put(CommonConstants.AUTHORIZATION,CommonConstants.authToken);
      Headers headers = Headers.of(requestHeaders);

      Request request = new Request.Builder().url(url).headers(headers).put(requestBody).build();

      Response response = httpClient.newCall(request).execute();

      if (!response.isSuccessful()) {
        log.error("response of HttpRequest failed due to http status :{}, at time :{}",
            response.code(), LocalDateTime.now());
      }
    } catch (Exception e) {
      log.error(
          "error while calling peoplehum api to upsert attendance logs due to :{}, at time :{}", e,
          LocalDateTime.now());
    }
  }
}
