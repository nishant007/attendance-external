package com.peoplehum.externalAttendance;

import com.peoplehum.externalAttendance.executor.AttendanceLogsPicker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.sql.Date;

@Slf4j
@SpringBootApplication
public class ExternalAttendanceApplication {

  // max. number of threads running when the queue is full
  @Value("${thread.pool.max:20}")
  private Integer maxThreadPool;
  // number of threads running at a time
  @Value("${thread.pool.max:10}")
  private Integer corePoolSize;

  static { System.setProperty("logback.configurationFile", "config/logback.xml");}

  public static void main(String[] args) {
    SpringApplication.run(ExternalAttendanceApplication.class, args);

    AttendanceLogsPicker attendanceLogsPicker = new AttendanceLogsPicker();
    if (args.length > 0) {
      String[] dates = args[0].split(",");
      switch (dates.length) {
        case 1:
          attendanceLogsPicker
              .loadAndSendAttendanceLogs(Date.valueOf(dates[0]), Date.valueOf(dates[0]));
          break;
        case 2:
          attendanceLogsPicker
              .loadAndSendAttendanceLogs(Date.valueOf(dates[0]), Date.valueOf(dates[1]));
          break;
      }
    } else {
      attendanceLogsPicker.attendanceLogsExecutor();
    }
  }

  @Bean(name = "defaultTaskExecutor")
  public ThreadPoolTaskExecutor defaultTaskExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(corePoolSize);
    executor.setMaxPoolSize(maxThreadPool);
    return executor;
  }
}
