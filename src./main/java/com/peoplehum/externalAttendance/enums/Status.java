package com.peoplehum.externalAttendance.enums;

public enum Status {
  Present("Present"),
  Absent("Absent"),
  WeeklyOffPresent("WeeklyOffPresent"),
  HalfDayPresent("1/2Present"),
  PRESENT("PRESENT"),
  ABSENT("ABSENT");

  private String value;

  Status(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
